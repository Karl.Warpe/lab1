package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

//import apple.laf.JRSUIConstants.Size;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        nextRound(); 
    }
    
    public void nextRound(){
        System.out.println("Let's play round " + roundCounter);
        String humanChoice = getHumanChoice(); 
        String computerChoice = getComputerChoice(); 
        String result = getResult(humanChoice, computerChoice); 
        System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". " + result); 
        System.out.println("Score: human " + humanScore + ", computer " + computerScore); 
        roundCounter++;
        promptUserContinue(); 
    }

    public String getHumanChoice(){
        String input = readInput("Your choice (Rock/Paper/Scissors)?").toString().toLowerCase();
        while (!rpsChoices.contains(input)){
            System.out.println("I do not understand " + input + ". Could you try again?"); 
            input = readInput("Your choice (Rock/Paper/Scissors)?").toString().toLowerCase(); 
        }
        return input; 
    }

    public String getComputerChoice(){
        Random r = new Random(); 
        int randomItem = r.nextInt(rpsChoices.size()); 
        return rpsChoices.get(randomItem); 
    }

    public String getResult(String human, String computer){
        String output = ""; 
        if (human.equals(computer)) {
            output = "It's a tie!"; 
        }
        else if ((rpsChoices.indexOf(human) + rpsChoices.size() - 1) % 3 == (rpsChoices.indexOf(computer) + rpsChoices.size()) % 3){
            output = "Human wins!"; 
            humanScore++;
        }
        else {
            output = "Computer wins!"; 
            computerScore++; 
        }
        return output; 
    }


    public void promptUserContinue(){
        String input = readInput("Do you wish to continue playing? (y/n)?").toString().toLowerCase();
        while (!"yn".contains(input)){
            System.out.println("I do not understand " + input + ". Could you try again?"); 
            input = readInput("Do you wish to continue playing? (y/n)?").toString().toLowerCase(); 
        }
        if (input.equals("y")){
            nextRound();
        } else if (input.equals("n")){
            System.out.println("Bye bye :)"); 
            System.exit(0); 
        }
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
